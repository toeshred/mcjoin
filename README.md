This script will alert an SMS email (or regular email) anytime a player connects
to a Minecraft server. You can even exclude specific players within the script.


Download the script with git:

    git clone https://gitlab.com/toeshred/mcjoin.git

Then run the script:

    cd mcjoin
    ./mcjoin &disown

Stopping the script can be done by just killing the process. If you don't know
how to stop a process, then just run it without the `&disown` part.
